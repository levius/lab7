import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Resolver implements Runnable {
    private NewVector vec;
    private SocketChannel channel;
    private DBHelper db;

    Resolver(SocketChannel channel, NewVector vec) {
        this.channel = channel;   //create object + channel
        this.vec = vec;
        db = new DBHelper();
        vec.vector = db.load();

    }

    @Override
    public void run() {
        try {
            List<Message> messages = new LinkedList<>();
            while (true) {
                byte[] b = new byte[65535];
                channel.read(ByteBuffer.wrap(b));
                int l = 0;
                for (int i = b.length - 1; i >= 0; i--) { // заменить на стрим апи
                    byte by = b[i];
                    if (by != 0) {
                        l = i;
                        break;
                    }
                }
                ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(b, 0, l + 1));// вытаскиваем байты в поток
                Object incoming = ois.readObject();
                if (incoming instanceof Message) {
                    messages.add((Message) incoming);
                    break;
                } else {
                    sendMessage("Неверный формат");
                    return;
                }
            }


            if (messages.size() == 1) {
                Message message = messages.get(0);
                System.out.println("Получено сообщение от " + channel.getLocalAddress() + ": " + message.getMessage());
                processMessage(message);
            } else {
                System.out.println("Получено " + messages.size() + " сообщений от " + channel.getRemoteAddress());
                for (int i = 0; i < messages.size(); i++)
                    processMessage(messages.get(i));
            }

        } catch (EOFException e) {
                System.out.println("Произошло непредвиденное окончание файла.");
                e.printStackTrace();
                sendMessage("Произошло непредвиденное окончание файла.");
        } catch (IOException e) {
            System.out.println("Ошибка выполнения: " + e.toString());
            sendMessage("Ошибка выполнения");
        } catch (ClassNotFoundException e) {
            sendMessage("Данные получены в неправильном формате!");
        }
    }


    private void sendMessage(String message) {
        try {
            byte[] byteArray = message.getBytes(UTF_8);
            ByteBuffer sendingBuffer = ByteBuffer.allocate(byteArray.length);// оаздаем байтбуфер нужного размера
            sendingBuffer.put(byteArray);
            sendingBuffer.flip();
            channel.write(sendingBuffer);
            ByteBuffer buf = ByteBuffer.allocate(1024);
            channel.read(buf);//
            buf.flip();
            channel.read(buf);//
            buf.flip();
            buf.clear();
            while (channel.read(buf) > 0) {
                buf.flip();
                System.out.println(new String(buf.array(), UTF_8));
                buf.clear();
            }

        } catch (IOException e) {
            System.out.println("Ошибка передачи данных клиенту.");
        }
    }//отправляет текстового сообщения на клиента

    private void processMessage(Message message) { // обработка команд которые приходят
        if (message == null) {
            sendMessage("Пустой запрос!");
            return;
        }
        switch (message.getMessage()) {
            case "login":

                String p = (message.getPassword());
                System.out.println(p);
                sendMessage(db.checkUser(message.getLogin(), p));
                break;
            case "register":
                String password = Password.generate(10);
                String sol = Password.generate(10);

                if(db.addUser(message.getLogin(), password, message.getEmail(), sol)){
                    MailSender.send(message.getEmail(), password);
                    sendMessage("true");
                }
                sendMessage("false");
            case "info":
                sendMessage(db.info());
                break;
            case "show":
                sendMessage(vec.show());
                break;
            case "save":
                try {
                    String path = System.getenv("lab5");
                    vec.saveCollection(path);
                    sendMessage("Сохранено!");
                } catch (FileNotFoundException e) {
                    sendMessage("Файл не найден!");
                } catch (IOException e) {
                    sendMessage("Проблемы с файлом!");
                }
                break;
            case "add":
                try {
                    System.out.println("add");
                    if (message.getPicture() == null) {
                        sendMessage("Ошибка ввода. Неверный формат. Не введена информация о картине.");
                        break;
                    }
                    vec.add_(message.getPicture());
                    db.AddPicture(message.getPicture());
                    sendMessage("Объект добавлен!");
                } catch (Exception e) {
                    sendMessage("Ошибка ввода. Неверный формат.");
                }
                break;
            case "insert":
                try {
                    if (message.getPicture() == null) {
                        sendMessage("Ошибка ввода. Неверный формат. Не введена информация о картине.");
                        break;
                    }
                    if (message.getIndex() == null) {
                        sendMessage("Ошибка ввода не задан индекс для команды insert");
                        break;
                    }
                    db.AddPicture(message.getPicture());
                    sendMessage(vec.add_(message.getIndex(), message.getPicture()));
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    sendMessage("Ошибка ввода. Неверный формат.");
                }
                break;
            case "remove":
                try {
                    if (message.getPicture() == null) {
                        sendMessage("Ошибка ввода. Неверный формат.  Не введена информация о картине.");
                        break;
                    }
                    boolean f = vec.remove_(message.getPicture());
                    System.out.println(f);
                    if (f) {
                        db.remove(message.getPicture());
                        sendMessage("Картина была удалена");
                    }else{
                        sendMessage("Невозможно удалить картину, которая вам не пренадлежит");
                    }
                } catch (Exception e) {
                    sendMessage("Ошибка ввода. Неверный формат.");
                }
                break;
            case "add_if_min":
                try {
                    if (message.getPicture() == null) {
                        sendMessage("Ошибка ввода. Неверный формат.  Не введена информация о картине.");
                        break;
                    }
                    if (vec.addIfMin(message.getPicture())) {
                        db.AddPicture(message.getPicture());
                        sendMessage("Картина добавлена");
                    }
                    sendMessage("Картина не соответствует условия добавления");

                } catch (Exception e) {
                    sendMessage("Ошибка ввода. Неверный формат.");
                }
                break;
            case "exit":
                db.active_users.remove(message.getLogin());
                System.out.println("Прекращение работы сервера");
                System.exit(-1);
            case "help":
                sendMessage(
                        "Доступные команды:\n" +
                                "insert int index {element}: добавить новый элемент с заданным индексом\n" +
                                "show: вывести в стандартный поток вывода все элементы коллекции в строковом представлении\n" +
                                "info: вывести в стандартный поток вывода информацию о коллекции (тип, дата инициализации, количество элементов и т.д.)\n" +
                                "add {element}: добавить в коллекцию картину\n" +
                                "add_if_min {element}: добавить в коллекцию картину если она меньше всех других\n" +
                                "remove {String key}: удалить элемент из коллекции по его значению");
                break;
            default:
                sendMessage("Неизвестная команда: " + message.getMessage());
        }
    }
}

