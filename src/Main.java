import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;

import java.net.UnknownHostException;
import java.nio.channels.NotYetBoundException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.UnresolvedAddressException;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {
public static int port;
    public static void main(String[] args) throws IOException {
        try {
            InetSocketAddress socketAddress = new InetSocketAddress(1);
            NewVector vector = new NewVector();
            Scanner sc = new Scanner(System.in);
            System.out.println("Добро пожаловать!");
            while(true) {
                try {
                    System.out.println("Введите номер порта");
                    Scanner sca = new Scanner(System.in);
                    port = sca.nextInt();
                    socketAddress = new InetSocketAddress(port);
                    break;
                }catch(InputMismatchException e){
                    System.out.println("Неверный формат порта");
                }catch (IllegalArgumentException e){
                    System.out.println("cлишком большой порт");}
            }


            ServerSocketChannel channel = null;
            try {
                channel = ServerSocketChannel.open(); // connect
                channel.socket().bind(socketAddress);// connect
                System.out.println("Готов к работе");
            } catch (IOException e) {
                System.out.println("Порт заблокирован: " + port);
                System.exit(-1);
            } catch (UnresolvedAddressException e) {
                System.out.println("Не удалось определить адрес сервера");
            }
             catch (SecurityException e) {
                System.out.println("Нет разрешения на подключение, проверьте свои настройки безопасности");
            } catch(InputMismatchException e ){
                System.out.println("Неверный порт");
            }catch (IllegalArgumentException e){
                System.out.println("порт аут оф ренж");

            }

            while (true) {
                try {
                    SocketChannel s = channel.accept();// connect
                    new Thread(new Resolver(s, vector)).start();
                } catch (IOException e) {
                    System.out.println("Ошибка подключения");
                }catch (NotYetBoundException e ){
                    System.out.println("порт аут оф рейнж");
                }
            }
        }catch(InputMismatchException e ){
            System.out.println("Неверный порт");
        }catch(NoSuchElementException e ){
            System.exit(-1);
        }
        }
}
