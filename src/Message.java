import java.io.Serializable;

public class Message<T extends Serializable> implements Serializable {
    private String command;
    private String filedata;
    private Integer index;
    private Picture picture;
    private String login;
    private String password;
    private String email;
    public Message(String command, String filedata, String login, String password, String email) {
        this.command = command;
        this.filedata = filedata;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public Message(String command, Picture picture,String login, String password, String email) {
        this.command = command;
        this.picture = picture;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public Message(String command, int index, Picture picture,String login, String password, String email) {
        this.command = command;
        this.index = index;
        this.picture = picture;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public Message(String command,String login,String password,String email) {
        this(command, "", login, password, email);
    }

    public String getMessage() {
        return command;
    }

    public Picture getPicture() {
        return picture;
    }

    public String getFiledata() {
        return filedata;
    }

    public Integer getIndex() {
        return index;
    }

    public String getLogin() { return login; }

    public String getPassword() { return password; }

    public String getEmail() { return email; }
}
