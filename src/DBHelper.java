import java.sql.*;
import java.time.LocalDateTime;
import java.util.Vector;
import java.util.concurrent.ConcurrentSkipListSet;

public class DBHelper {
    private Connection c;
    static PreparedStatement st;
    static Statement stmt;
    Vector<String> active_users = new Vector<String>();

    public DBHelper() {
        Vector<String> active_users = new Vector<String>();
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:1488/Levon", "postgres", "1111");
            System.out.println("База данных успешно подключена.");
            System.out.println(active_users);

        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Ошибка!");
            e.printStackTrace();
        }


    }


    public void AddPicture(Picture picture) {
        try {
            st = c.prepareStatement("INSERT INTO \"Exhibition\"(\"Picture\", \"Drawer\", \"Username\", \"Date\") VALUES (?,?,?,?);");
            st.setString(1, picture.getName());
            st.setString(2, picture.getCreator());
            st.setString(3, picture.getUsername());
            st.setString(4, picture.getDate());
            st.executeUpdate();
        } catch (SQLException s) {
            System.out.println("Ошибка!");
            s.printStackTrace();
        }
    }

    public void remove(Picture picture) {
        try {
            st = c.prepareStatement("DELETE FROM \"Exhibition\" WHERE \"Picture\"=? and \"Drawer\"=? and \"Username\"=?;");
            st.setString(1, picture.getName());
            st.setString(2, picture.getCreator());
            st.setString(3, picture.getUsername());
            st.executeUpdate();
        } catch (SQLException s) {
            s.printStackTrace();
            System.out.println("Ошибка!");

        }

    }

    public Vector<Picture> load() {
        Vector<Picture> pics = new Vector<Picture>();
        try {
            st = c.prepareStatement("SELECT * FROM \"Exhibition\";");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + rs.getString(2) + rs.getString(3));
                pics.add(new Picture(new Creator(rs.getString(2)), rs.getString(1), rs.getString(3), rs.getString(4)));
            }
            return pics;
        } catch (SQLException s) {
            System.out.println("Ошибка!");
            s.printStackTrace();
        }
        return null;
    }

    public String checkUser(String login, String password) {
        String s = "Ошибка авторизации: неверный логин/пароль!";
        try {
            st = c.prepareStatement("SELECT * FROM \"Users\" where \"Login\"=?;");
            st.setString(1, login);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                String passdb = rs.getString("Password");
                String sol = rs.getString("sol");
                String hash_pass = Hash.encrypt(sol + password);
                if (passdb.equals(hash_pass)) {
                    System.out.println(active_users);
                    System.out.println(active_users.contains(login));
                    if (!(active_users.contains(login))) {
                        s = "FOUND";
                        active_users.add(login);

                        System.out.println(active_users);
                    } else {
                        s = "LOGINED";
                    }
                } else {
                    s = "NOTFOUND";
                }
            }
            rs.close();
        } catch (Exception e) {

            System.out.println("Ошибка!");
            e.printStackTrace();
        }
        System.out.println(s);
        return s;
    }

    public boolean addUser(String login, String password, String email, String sol) {
        try {
            st = c.prepareStatement("SELECT * FROM \"Users\" where \"Login\"=?;");
            st.setString(1, login);
            ResultSet rs = st.executeQuery();
            if (!rs.next()){
                st = c.prepareStatement("SELECT * FROM \"Users\" where \"email\"=?;");
                st.setString(1, email);
                ResultSet rs1 = st.executeQuery();
                if(!rs1.next()){
                    st = c.prepareStatement("INSERT INTO \"Users\" VALUES (?,?,?,?);");
                    st.setString(1, login);
                    st.setString(2, Hash.encrypt(sol + password));
                    st.setString(3, email);
                    st.setString(4, sol);
                    st.executeUpdate();
                    st.close();
                    active_users.add(login);
                    return true;
                }else{
                    System.out.println("пользователь с такой почтой уже существует");
                    return false;

                }
            }else {
                System.out.println("Пользователь с таким логином уже сущействует");
                return false;
            }

        } catch (Exception e) {
            System.out.println("Ошибка!");
            e.printStackTrace();
        }
        return false;
    }

    public String info() {
        String i = "В базе данных ";
        try {
            st = c.prepareStatement("SELECT COUNT(\"Picture\") FROM \"Exhibition\";");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                i = i + rs.getInt(1) + " картины.\n";
            }
            st.close();
            st = c.prepareStatement("SELECT COUNT(\"Login\") FROM \"Users\";");
            rs = st.executeQuery();
            while (rs.next()) {
                i = i + "Зарегистрировано " + rs.getString(1) + " пользователей.\n";
            }
            st.close();
        } catch (Exception e) {
            System.out.println("Ошибка!");
        }
        return i;
    }


}
