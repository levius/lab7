import com.sun.mail.util.MailConnectException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import javax.mail.Message;

public class MailSender {
    public static void send(String email, String pass) {

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("l.pogosov13", "Richardleva11");
                    }
                });

        try {

            MimeMessage message;
            message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Password");
            message.setText("Ваш пароль для входа:  " + pass +"\n" +"В целях безопастности, не сообщайте этот пароль никому.");
            Transport.send(message);
            System.out.println("Пароль отправлен!");
        } catch (MessagingException e) {
            System.out.println("Не получилось отправить сообщение, проверьте подключение к сети интернет");
        }
    }
}
