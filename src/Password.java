import java.util.Random;

public class Password {
    public static String generate(int l){
        String all="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*_=+-/";
        String s="";
        Random random=new Random();
        for (int i=0; i<l; i++)
            s=s+all.charAt(random.nextInt(all.length()));
        return s;
    }
}
