import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class NewVector extends Vector<Picture> {
        Vector<Picture> vector = new Vector<>();
        int size;

        public static boolean checkFile(File file) {
            try {
                boolean exists = file.exists();
                if (exists) {
                    if (!file.canWrite()) {
                        System.out.println("Файл не может быть записан!");
                        return false;
                    } else {
                        if (!file.canRead()) {
                            System.out.println("Файл не может быть прочтен!");
                            return false;
                        } else if (!file.isFile()) {
                            System.out.println("Необходим файл, а не директория!");
                            return false;
                        } else
                            return true;
                    }
                } else {
                    System.out.println("Файл не существует!");
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }

        public void load(String filedata) throws IOException {
            try {
                 if (filedata != null && filedata.length() > 0) {
                    filedata = filedata.trim().replaceFirst("^([\\W]+)<","<");
                }
                System.out.println(filedata);
                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                Document doc = documentBuilder.parse(new ByteArrayInputStream(filedata.getBytes(StandardCharsets.UTF_8)));

                NodeList ListofPictures = doc.getElementsByTagName("picture");

                for (int i = 0; i < ListofPictures.getLength(); i++) {
                    Element pic = (Element) ListofPictures.item(i);
                    NodeList ListNames = pic.getElementsByTagName("name");
                    NodeList ListCreators = pic.getElementsByTagName("creator");
                    Element creator = (Element) ListCreators.item(0);
                    vector.add(new Picture(new Creator(ListCreators.item(0).getFirstChild().getNodeValue()), ListNames.item(0).getFirstChild().getNodeValue()));
                }
                size = vector.size();
                System.out.println("Файл успешно загружен");
            } catch (ParserConfigurationException e) {
                System.out.println("Ошибка структуры файлы");
            } catch (SAXException | IOException l) {
                System.out.println("Ошибка загрузки файла");
                l.printStackTrace();
            }
        }

        public void add_ (Picture picture){
            vector.add(picture);
        }

        public String add_(int index, Picture picture) {
            if (index < vector.size() - 1) {
                vector.insertElementAt(picture, index);
                return "Картина успешно вставлена";
            }else return "слишком большой индекс";
        }

        public boolean remove_(Picture picture) {
            int old = vector.size();
            System.out.println();
            vector.remove(picture);
            int now = vector.size();
            if(old==now){
                return false;
            }else{
                return true;
            }
        }

        public void saveCollection(String path) throws IOException {
            FileOutputStream stream = new FileOutputStream(path);
            BufferedOutputStream buf = new BufferedOutputStream(stream);
            Iterator<Picture> iterator = iterator();
            StringBuilder s = new StringBuilder();
            while (iterator.hasNext()) {
                Picture m = iterator.next();
                if (iterator.hasNext())
                    s.append(m.getName()).append(",").append(m.getCreator());
                else
                    s.append(m.getName()).append(",").append(m.getCreator());
            }
            buf.write(s.toString().getBytes());
            buf.close();
        }

        public  boolean addIfMin(Picture picture) throws IOException {
            Iterator<Picture> iterator = iterator();
            int a = 0;
            while (iterator.hasNext()) {
                if (picture.compareTo(iterator.next()) < 0)
                    a++;
            }
            if (a == size()) {
                add(picture);
                System.out.println("Объект добавлен!");
                return true;
            }
            else
                System.out.println("Объект не соответствует условию!");
            return false;
        }

        public String show() {
            System.out.println(vector.stream().toString());
            return Arrays.toString(vector.stream().sorted().toArray());
        }
    }
